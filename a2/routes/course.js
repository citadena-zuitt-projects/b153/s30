router.delete("/:courseId", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin){
	courseController.archiveCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}
})