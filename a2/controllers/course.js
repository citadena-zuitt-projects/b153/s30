module.exports.archiveCourse = (courseId, body) => {
	let archivedCourse = {
		isActive: false
	}

	return Course.findByIdAndUpdate(courseId, archivedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}